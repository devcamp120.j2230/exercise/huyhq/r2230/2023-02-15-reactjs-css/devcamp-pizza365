import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import img1 from './asset/images/1.jpg';
import img2 from './asset/images/2.jpg';
import img3 from './asset/images/3.jpg';
import img4 from './asset/images/4.jpg';
import imgSeafoot from './asset/images/seafood.jpg';
import imgHawaiian from './asset/images/hawaiian.jpg';
import imgBacon from './asset/images/bacon.jpg';

function App() {
  return (
    <div className='container-fluid'>
      {/* <!-- NAVIGATION AREA --> */}
      <nav className="navbar navbar-expand-lg navbar-light bg-warning">
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
          aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="row collapse navbar-collapse" id="navbarNav">
          <ul className="row col-12 navbar-nav text-center text-dark">
            <li className="nav-item active col-3">
              <a className="nav-link" href="#">Trang chủ <span className="sr-only">(current)</span></a>
            </li>
            <li className="nav-item col-3">
              <a className="nav-link" href="#size-pizza-choose">Combo</a>
            </li>
            <li className="nav-item col-3">
              <a className="nav-link" href="#type-pizza-choose">Loại Pizza</a>
            </li>
            <li className="nav-item col-3">
              <a className="nav-link" href="#order-create">Gửi đơn hàng</a>
            </li>
          </ul>
        </div>
      </nav>
      {/* <!-- BODY AREA --> */}
      <div className="container mt-5">
        <div className="mb-3">
          <h1 className="text-warning">PIZZA 365 </h1>
          <span className="text-warning font-italic">Truly italian!</span>
        </div>
        {/* <!-- SLIDE --> */}
        <div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">
          <ol className="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
          </ol>
          <div className="carousel-inner">
            <div className="carousel-item active">
              <img className="d-block w-100" src={img1} alt="First slide" />
            </div>
            <div className="carousel-item">
              <img className="d-block w-100" src={img2} alt="Second slide" />
            </div>
            <div className="carousel-item">
              <img className="d-block w-100" src={img3} alt="Third slide" />
            </div>
            <div className="carousel-item">
              <img className="d-block w-100" src={img4} alt="Fourth slide" />
            </div>
          </div>
          <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span className="carousel-control-prev-icon" aria-hidden="true"></span>
            <span className="sr-only">Previous</span>
          </a>
          <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span className="carousel-control-next-icon" aria-hidden="true"></span>
            <span className="sr-only">Next</span>
          </a>
        </div>
        {/* <!-- INFOMATION --> */}
        <div className=''>
          {/* Thông tin cửa hàng */}
          <div className="mt-5">
            <div className="text-warning text-center mb-3">
              <span className="h3 border-bottom border-warning"> Tại sao lại Pizza 365</span>
            </div>
            <div className="row">
              <div className="col-sm-12 col-md-3 p-5 border border-warning"
                style={{ backgroundColor: "lightgoldenrodyellow" }}>
                <h4 className="mb-3">Đa dạng</h4>
                <span>Số lượng pizza đa dạng, có đầy đủ các loại pizza đang hot nhất hiện nay.</span>
              </div>
              <div className="col-sm-12 col-md-3 p-5 border border-warning" style={{ backgroundColor: "yellow" }}>
                <h4 className="mb-3">Chất lượng</h4>
                <span>Nguyên liệu sạch 100% rõ nguồn gốc, quy trình chế biến đảm bảo vệ sinh an toàn thực
                  phẩm.</span>
              </div>
              <div className="col-sm-12 col-md-3 p-5 border border-warning" style={{ backgroundColor: "lightsalmon" }}>
                <h4 className="mb-3">Hương vị</h4>
                <span>Đảm bảo hương vị ngon, độc, lạ mà bạn chỉ có thể trải nghiệm từ Pizza 365.</span>
              </div>
              <div className="col-sm-12 col-md-3 p-5 border border-warning" style={{ backgroundColor: "orange" }}>
                <h4 className="mb-3">Dịch vụ</h4>
                <span>Nhân viên thân thiện, nhà hàng hiện đại. Dịch vụ giao hàng nhanh chất lượng, tân
                  tiến.</span>
              </div>
            </div>
          </div>
          {/* <!-- Chọn Size Pizza --> */}
          <div className="mt-5" id="size-pizza-choose">
            <div className="text-warning text-center mb-3">
              <span className="h3 border-bottom border-warning">Chọn size pizza</span>
              <p style={{color: "orange"}}>Chọn combo pizza phù hợp với nhu cầu của bạn.</p>
            </div>
            <div className="row text-center justify-content-center">
              <div className="row justify-content-center col-sm-12 col-md-4 mb-3 pl-3 pr-3 text-center">
                <div className="card" style={{width: "18rem"}}>
                  <div className="card-body p-0">
                    <h3 className="card-title p-3 size-pizza-title" style={{backgroundColor: "orange"}}>S (small)
                    </h3>
                    <ul className="list-group list-group-flush">
                      <li className="list-group-item">Đường kính: <b>20cm</b></li>
                      <li className="list-group-item">Sườn nướng: <b>2</b></li>
                      <li className="list-group-item">Salad: <b>200g</b></li>
                      <li className="list-group-item">Nước ngọt: <b>2</b></li>
                      <li className="list-group-item">
                        <b className="h3">150.000</b>
                        <p>VNĐ</p>
                      </li>
                      <li className="list-group-item bg-light">
                        <button className="btn col-12 font-weight-bold size-pizza-button"
                          data-is-selected-menu="N" id="small-choose"
                          style={{backgroundColor: "orange"}}>Chọn</button>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="row justify-content-center col-sm-12 col-md-4 mb-3 pl-3 pr-3 text-center ">
                <div className="card" style={{width: "18rem"}}>
                  <div className="card-body p-0">
                    <h3 className="card-title p-3 size-pizza-title" style={{backgroundColor: "orange"}}>M (medium)
                    </h3>
                    <ul className="list-group list-group-flush">
                      <li className="list-group-item">Đường kính: <b>25cm</b></li>
                      <li className="list-group-item">Sườn nướng: <b>4</b></li>
                      <li className="list-group-item">Salad: <b>300g</b></li>
                      <li className="list-group-item">Nước ngọt: <b>3</b></li>
                      <li className="list-group-item">
                        <b className="h3">200.000</b>
                        <p>VNĐ</p>
                      </li>
                      <li className="list-group-item bg-light">
                        <button className="btn col-12 font-weight-bold size-pizza-button"
                          data-is-selected-menu="N" id="medium-choose"
                          style={{backgroundColor: "orange"}}>Chọn</button>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="row justify-content-center col-sm-12 col-md-4 mb-3 pl-3 pr-3 text-center ">
                <div className="card" style={{width: "18rem"}}>
                  <div className="card-body p-0">
                    <h3 className="card-title p-3 size-pizza-title" style={{backgroundColor: "orange"}}>L (large)
                    </h3>
                    <ul className="list-group list-group-flush">
                      <li className="list-group-item">Đường kính: <b>30cm</b></li>
                      <li className="list-group-item">Sườn nướng: <b>8</b></li>
                      <li className="list-group-item">Salad: <b>500g</b></li>
                      <li className="list-group-item">Nước ngọt: <b>4</b></li>
                      <li className="list-group-item">
                        <b className="h3">250.000</b>
                        <p>VNĐ</p>
                      </li>
                      <li className="list-group-item bg-light">
                        <button className="btn col-12 font-weight-bold size-pizza-button"
                          data-is-selected-menu="N" id="large-choose"
                          style={{backgroundColor: "orange"}}>Chọn</button>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* <!-- Chọn loại Pizza --> */}
            <div className="mt-5" id="type-pizza-choose">
                <div className="text-warning text-center mb-3">
                    <span className="h3 border-bottom border-warning">Chọn loại pizza</span>
                </div>
                <div className="row justify-content-center">
                    <div className="row justify-content-center col-sm-12 col-md-4 p-3">
                        <div className="card" style={{width: "18rem"}}>
                            <img className="card-img-top" src={imgSeafoot} alt="Card image cap"/>
                            <div className="card-body">
                                <div className="card-title">
                                    <p className="h5">OCEAN MANIA</p>
                                    <p className="card-text">PIZZA HẢI SẢN XỐT MAYONNAISE</p>
                                </div>
                                <p className="card-text">Xốt Cà Chua, Phô Mai Mozzarella, Tôm Mực, Thanh Cua, Hành Tây.</p>
                                <div className="btn col-12 type-pizza-btn" style={{backgroundColor: "orange"}}
                                    data-is-selected-type-pizza="N" id="type-pizza-seafood">Chọn</div>
                            </div>
                        </div>
                    </div>
                    <div className="row justify-content-center col-sm-12 col-md-4 p-3">
                        <div className="card" style={{width: "18rem"}}>
                            <img className="card-img-top" src={imgHawaiian} alt="Card image cap"/>
                            <div className="card-body">
                                <div className="card-title">
                                    <p className="h5">HAWAIIAN</p>
                                    <p className="card-text">PIZZA DĂM BÔNG DỨA KIỂU HAWAII</p>
                                </div>
                                <p className="card-text">Xốt Cà Chua, Phô Mai Mozzarella, Thịt Dăm Bông, Thơm.</p>
                                <div className="btn col-12 type-pizza-btn" style={{backgroundColor: "orange"}}
                                    data-is-selected-type-pizza="N" id="type-pizza-hawaii">Chọn</div>
                            </div>
                        </div>
                    </div>
                    <div className="row justify-content-center col-sm-12 col-md-4 p-3">
                        <div className="card" style={{width: "18rem"}}>
                            <img className="card-img-top" src={imgBacon} alt="Card image cap"/>
                            <div className="card-body">
                                <div className="card-title">
                                    <p className="h5">CHEESY CHICKEN BACON</p>
                                    <p className="card-text">PIZZA GÀ PHÔ MAI THỊT HEO XÔNG KHÓI</p>
                                </div>
                                <p className="card-text">Xốt Cà Chua, Thịt gà, Thịt Heo Muối, Phô Mai Mozzarella.</p>
                                <div className="btn col-12 type-pizza-btn" style={{backgroundColor: "orange"}}
                                    data-is-selected-type-pizza="N" id="type-pizza-bacon">Chọn</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/* <!-- Chọn loại đồ uống --> */}
            <div className="mt-5">
                <div className="text-warning text-center mb-3">
                    <span className="h3 border-bottom border-warning">Chọn đồ uống</span>
                </div>
                <div className="m-5">
                    <select name="" id="select-drink" className="form-control form-control-md col-12 m-6"
                        aria-label="Default select example">
                        <option value="0">Tất cả các loại nước uống...</option>
                    </select>
                </div>
            </div>
            {/* <!-- Thông tin Order --> */}
            <div className="mt-5" id="order-create">
                <div className="text-warning text-center mb-3">
                    <span className="h3 border-bottom border-warning">Gửi đơn hàng</span>
                </div>
                <div>
                    <div className="row form-group ml-5 mr-5">
                        <label className="form-label col-12">Họ Tên</label>
                        <div className="col-12">
                            <input type="text" id="inp-name" className="form-control" placeholder="Nhập tên"/>
                        </div>
                    </div>
                    <div className="row form-group ml-5 mr-5">
                        <label className="form-label col-12">Email</label>
                        <div className="col-12">
                            <input type="text" id="inp-email" className="form-control" placeholder="Nhập email"/>
                        </div>
                    </div>
                    <div className="row form-group ml-5 mr-5">
                        <label className="form-label col-12">Số điện thoại</label>
                        <div className="col-12">
                            <input type="number" id="inp-phone" className="form-control" placeholder="Nhập số điện thoại"/>
                        </div>
                    </div>
                    <div className="row form-group ml-5 mr-5">
                        <label className="form-label col-12">Địa chỉ</label>
                        <div className="col-12">
                            <input type="text" id="inp-address" className="form-control" placeholder="Nhập địa chỉ"/>
                        </div>
                    </div>
                    <div className="row form-group ml-5 mr-5">
                        <label className="form-label col-12">Mã giảm giá</label>
                        <div className="col-12">
                            <input type="text" id="inp-discount" className="form-control" placeholder="Nhập mã giảm giá"/>
                        </div>
                    </div>
                    <div className="row form-group ml-5 mr-5">
                        <label className="form-label col-12">Lời nhắn</label>
                        <div className="col-12">
                            <input type="text" id="inp-message" className="form-control" placeholder="Nhập lời nhắn"/>
                        </div>
                    </div>
                    <div className="row form-group ml-5 mr-5">
                        <button className="btn font-weight-bold col-12" id="btn-order-detail"
                            style={{backgroundColor: "orange"}}>Gửi</button>
                    </div>
                </div>
            </div>
        </div>
      </div>
      {/* <!-- FOOTER AREA --> */}
    <div className="container-fluid mt-5 p-5 text-center" style={{backgroundColor: "orange"}}>
        <div className="m-3"> <b>Footer</b> </div>
        <div className="m-3">
            <a href="#" className="btn bg-dark text-light">
                <i className="fa-solid fa-arrow-up"></i>
                To the top
            </a>
        </div>
        <div className="m-3">
            <i className="fa-brands fa-facebook"></i>
            <i className="fa-brands fa-youtube"></i>
            <i className="fa-brands fa-twitter"></i>
        </div>
        <div className="m-3">
            <p>Hoàng Quốc Huy</p>
        </div>
    </div>
    </div>
  );
}

export default App;
